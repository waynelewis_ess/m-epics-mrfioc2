file "$(mrfioc2_TEMPLATES=db)/evr-pcie-300DC.db"
{
    {
    
        ## Global settings
		ExtInhib-Sel=0, 		#0 = Use Inhibit, 1 = Always permitt : Use HW trigger inhibit (EVRTG only)
		Link-Clk-SP=142.8,  	#50-150 : Event Link speed (MHz)
		Time-Src-Sel=0, 		#0 = Event clk, 1 = Mapped codes, 2 = DBus4 : Source for timestamping clock.
		Time-Clock-SP=0.0,		#50-150 : Timestamp tick rate (MHz)
		PLL-Bandwidth-Sel=4,	#0 = HM, 1 = HL, 2 = MH, 3 = MM, 4 = ML: PLL Bandwidth Select (see Silicon Labs Si5317 datasheet)

		## Delay compensation
		DlyCompensation-Enabled-Sel=1,	#0 = Disabled, 1 = Enabled: Enables or disables delay compensation
		DlyCompensation-Target-SP=1000,	# Delay compensation value in [ns]. Should be set to max(path + delta) of all receivers in the timing system.  This value can also be retrieved from an external record (see startup script example).
		DlyCompensation-Source="",      # delay compensation target value is sourced from the record referenced here. (default: empty)
		                                # Recommended setting for SwissFEL is SIN-CVME-TIMAST-TMA:EvrDC-SP
		DlyCompensation-Source-Disa=0,  #0 = Disabled, 1 = Enabled: enables or disables sourcing the delay compensation target value from record specified with 'DlyCompensation-Source' macro

		## Prescalers
		PS0-Div-SP=2, 			#2-4294967295 :Integer divisor between the Event Clock and the sub-unit output.
		PS0-PulserMap-L-SP=0,	#0-65535 :Trigger a pulser on prescaler rising edge. Pulsers 0-15 are bit-wise selectable
		PS0-PulserMap-H-SP=0,	#0-255   :Trigger a pulser on prescaler rising edge. Pulsers 16-23 are bit-wise selectable
		PS1-Div-SP=2, 			#2-4294967295 :Integer divisor between the Event Clock and the sub-unit output.
		PS1-PulserMap-L-SP=0,	#0-65535 :Trigger a pulser on prescaler rising edge. Pulsers 0-15 are bit-wise selectable
		PS1-PulserMap-H-SP=0,	#0-255   :Trigger a pulser on prescaler rising edge. Pulsers 16-23 are bit-wise selectable
		PS2-Div-SP=2, 			#2-4294967295 :Integer divisor between the Event Clock and the sub-unit output.
		PS2-PulserMap-L-SP=0,	#0-65535 :Trigger a pulser on prescaler rising edge. Pulsers 0-15 are bit-wise selectable
		PS2-PulserMap-H-SP=0,	#0-255   :Trigger a pulser on prescaler rising edge. Pulsers 16-23 are bit-wise selectable
		PS3-Div-SP=2, 			#2-4294967295 :Integer divisor between the Event Clock and the sub-unit output.
		PS3-PulserMap-L-SP=0,	#0-65535 :Trigger a pulser on prescaler rising edge. Pulsers 0-15 are bit-wise selectable
		PS3-PulserMap-H-SP=0,	#0-255   :Trigger a pulser on prescaler rising edge. Pulsers 16-23 are bit-wise selectable
		PS4-Div-SP=2, 			#2-4294967295 :Integer divisor between the Event Clock and the sub-unit output.
		PS4-PulserMap-L-SP=0,	#0-65535 :Trigger a pulser on prescaler rising edge. Pulsers 0-15 are bit-wise selectable
		PS4-PulserMap-H-SP=0,	#0-255   :Trigger a pulser on prescaler rising edge. Pulsers 16-23 are bit-wise selectable
		PS5-Div-SP=2, 			#2-4294967295 :Integer divisor between the Event Clock and the sub-unit output.
		PS5-PulserMap-L-SP=0,	#0-65535 :Trigger a pulser on prescaler rising edge. Pulsers 0-15 are bit-wise selectable
		PS5-PulserMap-H-SP=0,	#0-255   :Trigger a pulser on prescaler rising edge. Pulsers 16-23 are bit-wise selectable
		PS6-Div-SP=2, 			#2-4294967295 :Integer divisor between the Event Clock and the sub-unit output.
		PS6-PulserMap-L-SP=0,	#0-65535 :Trigger a pulser on prescaler rising edge. Pulsers 0-15 are bit-wise selectable
		PS6-PulserMap-H-SP=0,	#0-255   :Trigger a pulser on prescaler rising edge. Pulsers 16-23 are bit-wise selectable
		PS7-Div-SP=2, 			#2-4294967295 :Integer divisor between the Event Clock and the sub-unit output.
		PS7-PulserMap-L-SP=0,	#0-65535 :Trigger a pulser on prescaler rising edge. Pulsers 0-15 are bit-wise selectable
		PS7-PulserMap-H-SP=0,	#0-255   :Trigger a pulser on prescaler rising edge. Pulsers 16-23 are bit-wise selectable
			
		## Distributed bus
		PulserMap-Dbus0-SP=0,	#0-16777215 :Trigger a pulser on DBus bit rising edge. Pulsers 0-23 are bit-wise selectable
		PulserMap-Dbus1-SP=0,	#0-16777215 :Trigger a pulser on DBus bit rising edge. Pulsers 0-23 are bit-wise selectable
		PulserMap-Dbus2-SP=0,	#0-16777215 :Trigger a pulser on DBus bit rising edge. Pulsers 0-23 are bit-wise selectable
		PulserMap-Dbus3-SP=0,	#0-16777215 :Trigger a pulser on DBus bit rising edge. Pulsers 0-23 are bit-wise selectable
		PulserMap-Dbus4-SP=0,	#0-16777215 :Trigger a pulser on DBus bit rising edge. Pulsers 0-23 are bit-wise selectable
		PulserMap-Dbus5-SP=0,	#0-16777215 :Trigger a pulser on DBus bit rising edge. Pulsers 0-23 are bit-wise selectable
		PulserMap-Dbus6-SP=0,	#0-16777215 :Trigger a pulser on DBus bit rising edge. Pulsers 0-23 are bit-wise selectable
		PulserMap-Dbus7-SP=0,	#0-16777215 :Trigger a pulser on DBus bit rising edge. Pulsers 0-23 are bit-wise selectable
			

		## Pulsers
		Pul0-Ena-Sel=1,			#0 = Disabled, 1 = Enabled
		Pul0-Polarity-Sel=0,   	#0 = Active High, 1 = Active Low
		Pul0-Delay-SP=0,		#Pulse delay in us 
		Pul0-Width-SP=0,		#Pulse width in us
		Pul0-Prescaler-SP=1,   	#0-65535 : Pulser prescaler

		Pul1-Ena-Sel=1,			#0 = Disabled, 1 = Enabled
		Pul1-Polarity-Sel=0,   	#0 = Active High, 1 = Active Low
		Pul1-Delay-SP=0,		#Pulse delay in us 
		Pul1-Width-SP=0,		#Pulse width in us
		Pul1-Prescaler-SP=1,   	#0-65535 : Pulser prescaler

		Pul2-Ena-Sel=1,			#0 = Disabled, 1 = Enabled
		Pul2-Polarity-Sel=0,  	#0 = Active High, 1 = Active Low
		Pul2-Delay-SP=0,		#Pulse delay in us 
		Pul2-Width-SP=0,		#Pulse width in us
		Pul2-Prescaler-SP=1, 	#0-65535 : Pulser prescaler

		Pul3-Ena-Sel=1,			#0 = Disabled, 1 = Enabled
		Pul3-Polarity-Sel=0,   	#0 = Active High, 1 = Active Low
		Pul3-Delay-SP=0,		#Pulse delay in us 
		Pul3-Width-SP=0,		#Pulse width in us
		Pul3-Prescaler-SP=1,   	#0-65535 : Pulser prescaler

		Pul4-Ena-Sel=1,			#0 = Disabled, 1 = Enabled
		Pul4-Polarity-Sel=0,   	#0 = Active High, 1 = Active Low
		Pul4-Delay-SP=0,		#Pulse delay in us 
		Pul4-Width-SP=0,		#0-65535 : Width in us

		Pul5-Ena-Sel=1,			#0 = Disabled, 1 = Enabled
		Pul5-Polarity-Sel=0,   	#0 = Active High, 1 = Active Low
		Pul5-Delay-SP=0,		#Pulse delay in us 
		Pul5-Width-SP=0,		#0-65535 : Width in us

		Pul6-Ena-Sel=1,			#0 = Disabled, 1 = Enabled
		Pul6-Polarity-Sel=0,   	#0 = Active High, 1 = Active Low
		Pul6-Delay-SP=0,		#Pulse delay in us 
		Pul6-Width-SP=0,		#0-65535 : Width in us

		Pul7-Ena-Sel=1,			#0 = Disabled, 1 = Enabled
		Pul7-Polarity-Sel=0,   	#0 = Active High, 1 = Active Low
		Pul7-Delay-SP=0,		#Pulse delay in us 
		Pul7-Width-SP=0,		#0-65535 : Width in us

		Pul8-Ena-Sel=1,			#0 = Disabled, 1 = Enabled
		Pul8-Polarity-Sel=0,   	#0 = Active High, 1 = Active Low
		Pul8-Delay-SP=0,		#Pulse delay in us 
		Pul8-Width-SP=0,		#0-65535 : Width in us

		Pul9-Ena-Sel=1,			#0 = Disabled, 1 = Enabled
		Pul9-Polarity-Sel=0,   	#0 = Active High, 1 = Active Low
		Pul9-Delay-SP=0,		#Pulse delay in us 
		Pul9-Width-SP=0,		#0-65535 : Width in us

		Pul10-Ena-Sel=1,		#0 = Disabled, 1 = Enabled
		Pul10-Polarity-Sel=0,  	#0 = Active High, 1 = Active Low
		Pul10-Delay-SP=0,		#Pulse delay in us 
		Pul10-Width-SP=0,		#0-65535 : Width in us

		Pul11-Ena-Sel=1,		#0 = Disabled, 1 = Enabled
		Pul11-Polarity-Sel=0,  	#0 = Active High, 1 = Active Low
		Pul11-Delay-SP=0,		#Pulse delay in us 
		Pul11-Width-SP=0,		#0-65535 : Width in us

		Pul12-Ena-Sel=1,		#0 = Disabled, 1 = Enabled
		Pul12-Polarity-Sel=0,  	#0 = Active High, 1 = Active Low
		Pul12-Delay-SP=0,		#Pulse delay in us 
		Pul12-Width-SP=0,		#0-65535 : Width in us

		Pul13-Ena-Sel=1,		#0 = Disabled, 1 = Enabled
		Pul13-Polarity-Sel=0,  	#0 = Active High, 1 = Active Low
		Pul13-Delay-SP=0,		#Pulse delay in us 
		Pul13-Width-SP=0,		#0-65535 : Width in us

		Pul14-Ena-Sel=1,		#0 = Disabled, 1 = Enabled
		Pul14-Polarity-Sel=0,  	#0 = Active High, 1 = Active Low
		Pul14-Delay-SP=0,		#Pulse delay in us 
		Pul14-Width-SP=0,		#0-65535 : Width in us

		Pul15-Ena-Sel=1,		#0 = Disabled, 1 = Enabled
		Pul15-Polarity-Sel=0,  	#0 = Active High, 1 = Active Low
		Pul15-Delay-SP=0,		#Pulse delay in us 
		Pul15-Width-SP=0,		#0-65535 : Width in us


		## Front panel outputs
		# Available -Src-SP choices:
		# 0 	Pulser 0
		# 1 	Pulser 1
		# 2 	Pulser 2
		# 3 	Pulser 3
		# 4 	Pulser 4
		# 5 	Pulser 5
		# 6 	Pulser 6
		# 7 	Pulser 7
		# 8 	Pulser 8
		# 9 	Pulser 9
		# 10 	Pulser 10
		# 11 	Pulser 11
		# 12 	Pulser 12
		# 13 	Pulser 13
		# 14 	Pulser 14
		# 15 	Pulser 15
		# 32	DBus 0
		# 33	DBus 1
		# 34	DBus 2
		# 35	DBus 3
		# 36	DBus 4
		# 37	DBus 5	
		# 38	DBus 6
		# 39	Dbus 7
		# 40	Prescaler 0
		# 41	Prescaler 1
		# 42	Prescaler 2
		# 43	Prescaler 3
		# 44	Prescaler 4
		# 45	Prescaler 5
		# 46	Prescaler 6
		# 47	Prescaler 7
		# 63	Force Low
		# 62	Force High


		# Universal outputs
		#  Slot 0
		FrontUnivOut0-Ena-SP=1, 	# Default to enabled
		FrontUnivOut0-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut0-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		FrontUnivOut1-Ena-SP=1, 	# Default to enabled
		FrontUnivOut1-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut1-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		#  Slot 1
		FrontUnivOut2-Ena-SP=1, 	# Default to enabled
		FrontUnivOut2-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut2-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		FrontUnivOut3-Ena-SP=1, 	# Default to enabled
		FrontUnivOut3-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut3-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		#  Slot 2
		FrontUnivOut4-Ena-SP=1, 	# Default to enabled
		FrontUnivOut4-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut4-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		FrontUnivOut5-Ena-SP=1, 	# Default to enabled
		FrontUnivOut5-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut5-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		#  Slot 3
		FrontUnivOut6-Ena-SP=1, 	# Default to enabled
		FrontUnivOut6-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut6-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		FrontUnivOut7-Ena-SP=1, 	# Default to enabled
		FrontUnivOut7-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut7-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		#  Slot 4
		FrontUnivOut8-Ena-SP=1, 	# Default to enabled
		FrontUnivOut8-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut8-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		FrontUnivOut9-Ena-SP=1, 	# Default to enabled
		FrontUnivOut9-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut9-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		#  Slot 5
		FrontUnivOut10-Ena-SP=1, 	# Default to enabled
		FrontUnivOut10-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut10-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		FrontUnivOut11-Ena-SP=1, 	# Default to enabled
		FrontUnivOut11-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut11-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		#  Slot 6
		FrontUnivOut12-Ena-SP=1, 	# Default to enabled
		FrontUnivOut12-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut12-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		FrontUnivOut13-Ena-SP=1, 	# Default to enabled
		FrontUnivOut13-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut13-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		#  Slot 7
		FrontUnivOut14-Ena-SP=1, 	# Default to enabled
		FrontUnivOut14-Src-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut14-Src2-SP=63,  	# Defaults to Force low (if enabled -> Ena-SP)

		FrontUnivOut15-Ena-SP=1, 	# Default to enabled
		FrontUnivOut15-Src-SP=63  	# Defaults to Force low (if enabled -> Ena-SP)
		FrontUnivOut15-Src2-SP=63  	# Defaults to Force low (if enabled -> Ena-SP)

     }
 }

## Mapping between hardware event code and a software (EPICS) database event
## In addition to firing an EPICS database event on each occurance of hardware event, a counter for the specific EVT is increased.
#
# Macros:
#  SYS = System name (auto expanded by parent)
#  DEVICE = Event receiver / timing card name (same as mrmEvrSetupVME()) Eg. EVR0. (auto expanded by parent)
#  EVT = Event code (hardware). Set EVT=0 to disable.
#  CODE = EPICS database event number (software)
#  FLNK = If provided, forward links to the record after all records from this template are processed
#
file "$(mrfioc2_TEMPLATES=db)/evr-softEvent.template"{
pattern { EVT,	CODE,    FLNK }
		{ "1",    "1",   "" }
		{ "2",    "2",   "" }
		{ "3",    "3",   "" }
}
# If you want to enable measuring the jitter on received events comment out the previous substitution and use this one instead
# 
# Additinal macros:
#  HIST_LEN = number of histogram bins created from frequency measurements. Default 200.
#  HIST_ULIM = histogram upper limit. Default 12.
#  HIST_LLIM = histogram lower limit. Default 8.
# 
# NOTE: make sure your system supports the 'histogram' record before using this template!
#  
#file "$(mrfioc2_TEMPLATES=db)/evr-softEvent-measure.template"{
#pattern { EVT,	CODE,    FLNK }
#		{ "1",    "1",   "" }
#		{ "2",    "2",   "" }
#		{ "3",    "3",   "" }
#}

## Mapping between hardware event code and a special function of the EVR
## Each event can be mapped only to one function!
## There are some default events, that allways trigger specific functions!
## Available functions:
##		- Blink : An LED on the EVRs front panel will blink when the code is received.
##		- Forward : The received code will be immediately retransmitted on the upstream event link.
##		- Stop Log (default event 121): Freeze the circular event log buffer. An CPU interrupt will be raised which will cause the buffer to be downloaded. This might be a useful action to map to a fault event.
##		- Log : Include this event code in the circular event log.
##		- Heartbeat (default event 122): This event resets the heartbeat timeout timer.
##		- Reset PS (default event 123): Resets the phase of all prescalers.
##		- TS reset (default event 124): Transfers the seconds timestamp from the shift register and zeros the sub-seconds part.
##		- TS tick (default event 125): When the timestamp source is 'Mapped code' then any event with this mapping will cause the sub-seconds part of the timestamp to increment.
##		- Shift 1 (default event 113): Shifts the current value of the seconds timestamp shift register up by one bit and sets the low bit to 1.
##		- Shift 0  (default event 112): Shifts the current value of the seconds timestamp shift register up by one bit and sets the low bit to 0.
##		- FIFO : Bypass the automatic allocation mechanism and always include this code in the event FIFO.
#
# Macros:
#  SYS = System name (auto expanded by parent)
#  DEVICE = Event receiver / timing card name (same as mrmEvrSetupVME()) Eg. EVR0. (auto expanded by parent)
#  EVT = Event code (hardware). Set EVNT=0 to disable.
#  FUNC = Function to be mapped
#    Choices are: "FIFO", "Latch TS", "Blink", "Forward",
#                 "Stop Log", "Log", "Heartbeat", "Reset PS",
#                 "TS reset", "TS tick", "Shift 1", "Shift 0",
#
file "$(mrfioc2_TEMPLATES=db)/evr-specialFunctionMap.template"{
pattern { EVT,   FUNC }
        {"1",   "Blink"}
}

## Control for mapping a pulse geneator to an event code in hardware.
## Each pulser-function combination can be mapped to multiple events.
## Available functions:
##		- Trig : causes a received event to trigger a pulser. A pulse based on pulser polarity, delay and width will be outputted.
##		- Set  : causes a received event to force a pulser into set state(based on pulser polarity).
##		- Reset: causes a received event to force a pulser into reset state(based on pulser polarity).
#
## Pulser polarity defines the pulser set and reset state:
##		- Active high: Output is logic high in set state and logic low in reset state
##		- Active low: Output is logic low in set state and logic high in reset state
#
# Macros:
#  SYS = system name (auto expanded by parent)
#  DEVICE = Event receiver / timing card name (same as mrmEvrSetupVME()) Eg. EVR0. (auto expanded by parent)
#  PID = Pulser ID #
#  F = Pulse generator function
#    Choices are: "Trig", "Set", "Reset"
#  EVT = Initial event code
#  ID = Mappings must have unique ID for each PID-F combination.
#    Only mappings with ID=0 are displayed in the GUI
#    
file "$(mrfioc2_TEMPLATES=db)/evr-pulserMap.template"{
pattern {PID   F,      EVT, ID}
        {0,    Trig,   0,   0 }
        {1,    Trig,   0,   0 }
        {2,    Trig,   0,   0 }
        {3,    Trig,   0,   0 }
        {4,    Trig,   0,   0 }
        {5,    Trig,   0,   0 }
        {6,    Trig,   0,   0 }
        {7,    Trig,   0,   0 }
        {8,    Trig,   0,   0 }
        {9,    Trig,   0,   0 }
        {10,   Trig,   0,   0 }
        {11,   Trig,   0,   0 }
        {12,   Trig,   0,   0 }
        {13,   Trig,   0,   0 }
        {14,   Trig,   0,   0 }
        {15,   Trig,   0,   0 }


        {0,    Set,   0,   0 }
        {1,    Set,   0,   0 }
        {2,    Set,   0,   0 }
        {3,    Set,   0,   0 }
        {4,    Set,   0,   0 }
        {5,    Set,   0,   0 }
        {6,    Set,   0,   0 }
        {7,    Set,   0,   0 }
        {8,    Set,   0,   0 }
        {9,    Set,   0,   0 }
        {10,   Set,   0,   0 }
        {11,   Set,   0,   0 }
        {12,   Set,   0,   0 }
        {13,   Set,   0,   0 }
        {14,   Set,   0,   0 }
        {15,   Set,   0,   0 }


        {0,    Reset,   0,   0 }
        {1,    Reset,   0,   0 }
        {2,    Reset,   0,   0 }
        {3,    Reset,   0,   0 }
        {4,    Reset,   0,   0 }
        {5,    Reset,   0,   0 }
        {6,    Reset,   0,   0 }
        {7,    Reset,   0,   0 }
        {8,    Reset,   0,   0 }
        {9,    Reset,   0,   0 }
        {10,   Reset,   0,   0 }
        {11,   Reset,   0,   0 }
        {12,   Reset,   0,   0 }
        {13,   Reset,   0,   0 }
        {14,   Reset,   0,   0 }
        {15,   Reset,   0,   0 }
}

## Flash access support
## Uncomment this substitution to load records that expose read and write access to the flash chip on the device.
#
# Macros:
#  SYS = System name (auto expanded by parent)
#  DEVICE = Event receiver / timing card name (same as mrmEvrSetupVME()) Eg. EVR0. (auto expanded by parent)
# 
#file "$(mrfioc2_TEMPLATES=db)/flash.template" 
#{
#	{}
#}
